```go
func Constructor(f, l string) Student {
	obj := Student{sap: 500000000, roll: "R1XXXXXXXX", p: Person{fName: f, lName: l}}
	return obj
}

// List of functions
func NewUser(firstName, lastName string) *User {
	return &User{FirstName: firstName,
		LastName: lastName,
		Location: &UserLocation{
			City:    "Santa Monica",
			Country: "USA",
		},
	}
}

```