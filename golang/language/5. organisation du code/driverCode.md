```go
func main() {

	// Create a person object
	person1 := Person{fName: "Karan", lName: "Nautiyal"}
	fmt.Println(person1)
	fmt.Println(person1.Greetings())

	// Create a student object
	student1 := Student{sap: 500060720, roll: "R171217041", p: Person{fName: "Nishkarsh", lName: "Raj"}}
	fmt.Println(student1)
	// Composition allows child struct to call method of base struct
	fmt.Println(student1.p.Greetings())

	// Create student with Create function
	student2 := Constructor("Megha", "Rawat")
	fmt.Println(student2)
}
```