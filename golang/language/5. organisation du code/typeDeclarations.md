```go
type Person struct {
	fName, lName string
}

type Student struct {
	sap  int
	roll string
	p    Person
}

// Main type(s) for the file,
// try to keep the lowest amount of structs per file when possible.
type User struct {
	FirstName, LastName string
	Location            *UserLocation
}

type UserLocation struct {
	City    string
	Country string
}

```