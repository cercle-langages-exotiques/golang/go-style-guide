```go
var (
	Exported    = "Hello, World!"
	nonExported = "Bye!"
)

// list of variables
var (
	ExportedVar    = 42
	nonExportedVar = "so say we all"
)
```