```go
func (p Person) Greetings() string {
	return fmt.Sprintf("Hello %s\n", p.fName)
}


// List of methods
func (u *User) Greeting() string {
	return fmt.Sprintf("Dear %s %s", u.FirstName, u.LastName)
}

func main() {
	us := User{FirstName: "Matt",
		LastName: "Damon",
		Location: &UserLocation{
			City:    "Santa Monica",
			Country: "USA"}}
	fmt.Println(us.Greeting())
}

```